<?php

use Illuminate\Database\Seeder;

class ReadingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i = 100; $i++) {
            \App\Reading::create([
                'temperature' => rand(5, 19)
            ]);
        }
    }
}
